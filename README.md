# Traffic Counter
The purpose of this project to count requests from different servers based on the packet patterns send over the network 
between the two machines.
This project is currently in the phase of exploring and is successfully working for RESTful API's, MySQL and MongoDB.

### Prerequisites

What things you need to install and how to install them.

````
1) Java version 1.8 or higher.
   You can download the latest version at: https://www.java.com/nl/download/

2) Maven
   You can download Maven at: https://maven.apache.org/
````

### Getting started

One of the used libraries requires administrator/root privileges. As noted by the Pcap4J development team for linux users:
````
"[...]If on Linux, you can run Pcap4J with a non-root user by granting capabilities CAP_NET_RAW and CAP_NET_ADMIN to 
your java command by the following command: setcap cap_net_raw,cap_net_admin=eip /path/to/java"
````
When on windows, one needs to run the tool  with administrator privileges.

### Installing

1) Clone the repository
2) Fill in the network.properties properly.
   ````
   sources-list=[any custom name]
   source.[any custom name].ip=[ip of the source]
   source.[any custom name].port[port the source uses]
   ````
   
   For example, you have an api gateway running on port 8081 and expect another machine with IP 127.0.0.2 performing 
   some requests, it should look as follows:
   ````
   sources-list=api-gateway
   source.api-gateway.ip=127.0.0.2
   source.api-gateway.port=8081
   ````
   
   If you expect more machines to make use of your api-gateway on port 8081, simply add the new ip (lets pretend it is 127.0.0.3)
   ````
   source.api-gateway.ip=127.0.0.2,127.0.0.3
   ````
   If you want to listen to more services, such as the api-gateway, but also expect someone to reach you MySQL server on the default port 3306 change the following:
   ````
   sources-list=api-gateway,mysql
   source.api-gateway.ip=127.0.0.2
   source.api-gateway.port=8081
   source.mysql.ip=127.0.0.2
   source.mysql.port=3306
   ````
   
1) run the command "mvn clean compile"

### Running the tests

Locally, unit test can be easily run with Maven using the following command:
````
mvn test
````
### Deployment

To deploy one first have to build the project:
````
mvn package
````
Subsequently, distribute the jar that you can find in the target folder/machine to any location you like.
Run the application with:
````
java -jar [application name].jar
````

Make sure you have the network.properties configuration set up properly.

### Build with
*[Maven](https://maven.apache.org/) - Dependency Management
*[Gitlab CI](https://about.gitlab.com/features/gitlab-ci-cd/) - Automatic build and test *

### Authors

Christian martin - Initial work - [GitLab](https://gitlab.com/crittje) - [LinkedIn](https://www.linkedin.com/in/christianmartin5/)

### License

This project is licensed under the MIT License - see the LICENSE.md file for details



### Acknowledgments

[Kaitoy](https://github.com/kaitoy) - for being repsonsible for such a nice library to handle packets and supporting it.

