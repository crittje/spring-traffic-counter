package com.qnext.trafficcounter.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.*;

public class ListUtilsTest {

    private ListUtils utils = new ListUtils();
    private LinkedList<Long> longList = new LinkedList<>();
    private LinkedList<Character> charList = new LinkedList<>();

    @Before
    public void setUp() {
        longList.add(1L);
        longList.add(2L);
        longList.add(3L);

        charList.add('A');
        charList.add('B');
        charList.add('C');
    }

    @Test
    public void clearListExceptLastLongTest() {

        utils.clearListExceptLastLong(longList);
        assertEquals(1, longList.size());
    }

    @Test
    public void clearListExceptLastStringTest() {

        utils.clearListExceptLastString(charList);
        assertEquals(1, charList.size());
    }
}