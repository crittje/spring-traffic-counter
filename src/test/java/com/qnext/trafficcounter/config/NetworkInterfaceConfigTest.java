package com.qnext.trafficcounter.config;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class NetworkInterfaceConfigTest {

    final private String IP = "127.0.0.1";

    private NetworkInterfaceConfig networkInterfaceConfig = new NetworkInterfaceConfig();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        networkInterfaceConfig.getSourcesMap().clear();
        networkInterfaceConfig.getIpSet().clear();
    }

    @Test
    public void modifySourceMapTest() {

        networkInterfaceConfig.modifySourceMap(IP, 8000);

        assertEquals(1, networkInterfaceConfig.getSourcesMap().size());
    }

    @Test
    public void addIdenticalSourceTest() {

        networkInterfaceConfig.modifySourceMap(IP, 8000);
        networkInterfaceConfig.modifySourceMap(IP, 8000);

        assertEquals(1, networkInterfaceConfig.getSourcesMap().size());
    }

    @Test
    public void addNewSourceTest() {

        networkInterfaceConfig.modifySourceMap(IP, 8001);
        networkInterfaceConfig.modifySourceMap(IP, 8002);

        assertEquals(1, networkInterfaceConfig.getSourcesMap().size());

    }

    @Test
    public void addNewSourceDifferentPortTest() {

        networkInterfaceConfig.modifySourceMap(IP, 8001);
        networkInterfaceConfig.modifySourceMap(IP, 8002);

        assertEquals(2, networkInterfaceConfig.getSourcesMap().get(IP).size());
    }
}