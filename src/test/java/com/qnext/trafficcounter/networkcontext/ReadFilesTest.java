package com.qnext.trafficcounter.networkcontext;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.util.Properties;

import static org.junit.Assert.*;

public class ReadFilesTest {

    private ReadFiles readFiles = new ReadFiles();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void configFilesTest() throws FileNotFoundException {
        Properties properties = readFiles.configFiles("config/network.properties");
        assertNotEquals(0, properties.size());
    }

    @Test
    public void testNonExistingFileException() throws FileNotFoundException {
        expectedException.expect(FileNotFoundException.class);
        expectedException.expectMessage("Cannot find \'network.properties\' file. Shutting down");

        readFiles.configFiles("none");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwMessageIfInvalidPropertyTest() {
        Properties properties = new Properties();
        properties.put("source", "one");
        properties.put("sourceTwo", "");

        readFiles.validateProperties(properties);
    }
}