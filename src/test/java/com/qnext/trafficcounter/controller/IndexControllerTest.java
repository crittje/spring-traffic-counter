package com.qnext.trafficcounter.controller;

import com.qnext.trafficcounter.domain.Request;
import com.qnext.trafficcounter.services.RequestService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class IndexControllerTest {


    @Mock
    private RequestService requestService;

    @InjectMocks
    private IndexController indexController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(indexController).build();
    }

    @Test
    public void getRequestTest() throws Exception {

        // Given
        final String IP = "127.0.0.1";

        Request request1 = new Request();
        request1.setSourceIp(IP);
        request1.setPort(8000);
        request1.setCount(10L);

        Request request2 = new Request();
        request2.setSourceIp(IP);
        request2.setPort(7000);
        request2.setCount(20L);

        List<Request> requestList = new ArrayList<>();
        requestList.add(request1);
        requestList.add(request2);

        when(requestService.getRequests()).thenReturn(requestList);

        mockMvc.perform(get("/req")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}