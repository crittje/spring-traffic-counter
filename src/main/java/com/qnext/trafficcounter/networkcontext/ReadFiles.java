package com.qnext.trafficcounter.networkcontext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class ReadFiles {

    private Logger logger = LogManager.getLogger();

    public Properties configFiles(final String FILE_LOCATION) throws FileNotFoundException {

        Properties properties = new Properties();
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(FILE_LOCATION);
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error("Cannot find file {}" + FILE_LOCATION);
            throw new FileNotFoundException("Cannot find \'network.properties\' file. Shutting down");
        }
        finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }

        validateProperties(properties);

        return properties;
    }

    public void validateProperties(Properties properties) {

        Set<String> propertyNames = properties.stringPropertyNames();
        for (String property : propertyNames) {
            String propertyValue = properties.getProperty(property);
            if (propertyValue.isEmpty()) {
                logger.fatal("Property \'{}\' in \'config/network.properties\' is empty, shutting down app", property);
                throw new IllegalArgumentException(String.format("Property \'{}\' in \'config/network.properties\' " +
                        "is empty, shutting down app", property));
            }
        }
    }
}
