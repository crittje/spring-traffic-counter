package com.qnext.trafficcounter.networkcontext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pcap4j.core.*;

import java.util.concurrent.atomic.AtomicInteger;

public class InitListener implements Runnable {

    private final Logger logger = LogManager.getLogger();

    private int port;
    private String ip;
    private final PcapNetworkInterface NETWORK_INTERFACE;
    private PacketLists packetLists = new PacketLists();
    private AtomicInteger countRequests = new AtomicInteger();

    InitListener(int port, String ip, PcapNetworkInterface networkInterface) {
        this.port = port;
        this.ip = ip;
        NETWORK_INTERFACE = networkInterface;
    }

    @Override
    public void run() {
        logger.debug("Starting new thread to listen to packets on ip {} and port {}", ip, port);
        try {
            setUpPacketListener();
        } catch (PcapNativeException | NotOpenException | InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    private void setUpPacketListener() throws PcapNativeException, NotOpenException, InterruptedException {
        logger.trace("In setUpPacketListener method");

        final int SNAP_LEN = 65536;
        final int TIMEOUT = 10;

        PcapHandle pcapHandle = NETWORK_INTERFACE.openLive(SNAP_LEN, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS,
                TIMEOUT);

        HandlePackets packetListener = new HandlePackets(ip, port, getCountRequests(), packetLists);

        pcapHandle.loop(0, packetListener);
    }

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public AtomicInteger getCountRequests() {
        return countRequests;
    }

}
