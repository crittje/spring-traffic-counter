package com.qnext.trafficcounter.networkcontext;

import java.util.LinkedList;

public class PacketLists {

    private LinkedList<Long> sequenceList;
    private LinkedList<Long> ackNoList;
    private LinkedList<Character> patternList;

    PacketLists() {
        sequenceList = new LinkedList<>();
        ackNoList = new LinkedList<>();
        patternList = new LinkedList<>();
    }

    public LinkedList<Long> getSequenceList() {
        return sequenceList;
    }

    public LinkedList<Long> getAckNoList() {
        return ackNoList;
    }

    public LinkedList<Character> getPatternList() {
        return patternList;
    }
}
