package com.qnext.trafficcounter.networkcontext;

import com.qnext.trafficcounter.config.NetworkInterfaceConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pcap4j.core.PcapNativeException;
import org.pcap4j.core.PcapNetworkInterface;
import org.pcap4j.core.Pcaps;

import java.util.*;

public class NetworkContext {

    final private Logger logger = LogManager.getLogger();
    final private PcapNetworkInterface nif;
    final private Iterator<Map.Entry<String, LinkedList<Integer>>> sourceIterator;
    private List<InitListener> listenersList = new ArrayList<>();

    public NetworkContext() {
        nif = getNetworkInterfaces();

        NetworkInterfaceConfig networkConfiguration = new NetworkInterfaceConfig();
        sourceIterator = networkConfiguration.getSourcesMap().entrySet().iterator();
        startContext();
    }

    private PcapNetworkInterface getNetworkInterfaces() {

        List<PcapNetworkInterface> devices = null;
        try {
            devices = Pcaps.findAllDevs();
        } catch (PcapNativeException e) {
            logger.error(e.getMessage());
            System.exit(-1);
        }

        return devices.stream()
                .filter(device -> device.getName().equalsIgnoreCase("ANY"))
                .findAny()
                .orElse(null);
    }

    private void startContext() {

        while (sourceIterator.hasNext()) {
            Map.Entry<String, LinkedList<Integer>> pair = sourceIterator.next();
            String ip = pair.getKey();

            pair.getValue()
                    .forEach(port -> {
                        InitListener initListener = new InitListener(port, ip, nif);
                        getListenersList().add(initListener);
                        new Thread(initListener).start();

                    });
        }
    }

    public List<InitListener> getListenersList() {
        return listenersList;
    }

    public void setListenersList(List<InitListener> listenersList) {
        this.listenersList = listenersList;
    }
}
