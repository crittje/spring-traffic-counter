package com.qnext.trafficcounter.networkcontext;

import com.qnext.trafficcounter.utils.ListUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pcap4j.core.PacketListener;
import org.pcap4j.packet.Packet;
import org.pcap4j.packet.TcpPacket;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

public class HandlePackets implements PacketListener {

    private Logger logger = LogManager.getLogger();
    private String sourceIp;
    private int destinationPort;
    private LinkedList<Long> sequenceList;
    private LinkedList<Long> ackNoList;
    private LinkedList<Character> patternList;
    private Packet packet;
    private ListUtils listUtils = new ListUtils();
    private AtomicInteger countRequests;

    HandlePackets(String sourceIp, int destinationPort, AtomicInteger countRequests, PacketLists packetLists) {
        this.sourceIp = sourceIp;
        this.destinationPort = destinationPort;
        this.countRequests = countRequests;
        sequenceList = packetLists.getSequenceList();
        ackNoList = packetLists.getAckNoList();
        patternList = packetLists.getPatternList();
    }

    @Override
    public void gotPacket(Packet packet) {

        logger.trace("new gotPacket event");
        this.packet = packet;

        retrieveTcpInformation(packet.get(TcpPacket.class));
    }

    private void retrieveTcpInformation(TcpPacket tcpPacket) {
        logger.trace("In retrieveTcpInformation method");
        TcpPacket.TcpHeader tcpHeader;

        try {
            tcpHeader = tcpPacket.getHeader();
        } catch (NullPointerException e) {
            logger.trace("Null at tcpHeader");
            return;
        }
        if (tcpHeader.getDstPort().valueAsInt() == destinationPort) {
            logger.trace("+++++ NEW EVENT +++++");
            logger.trace(packet);

            identifyPacketType(tcpHeader);

            int size = sequenceList.size();
            if (size > 2) {
                int threeElementsBack = size - 3;
                int twoElementsBack = size - 2;

                long prevSeq = sequenceList.getLast();
                long prevAck = ackNoList.getLast();
                long twoSeqBack = sequenceList.get(twoElementsBack);
                long twoAckBack = ackNoList.get(twoElementsBack);
                long threeSeqBack = sequenceList.get(threeElementsBack);
                long threeAckBack = ackNoList.get(threeElementsBack);
                long sequenceNoOccurrence = determineElementOccurrence(sequenceList, twoSeqBack);
                long ackNoOccurrence = determineElementOccurrence(ackNoList, twoAckBack);

                if ((sequenceNoOccurrence > 1) && (ackNoOccurrence > 1)) {
                    if ((twoAckBack == threeAckBack) && (twoSeqBack == threeSeqBack)) {
                        comparePackets(twoElementsBack, threeElementsBack);
                    } else if ((twoAckBack == prevAck) && (twoSeqBack == prevSeq)) {
                        comparePackets(twoElementsBack, threeElementsBack);
                    } else {
                        comparePackets(twoElementsBack, threeElementsBack);
                    }
                }
                logger.debug("Request count = {} on {}:{}", countRequests.get(), sourceIp, destinationPort);
            }
        }
    }

    private void identifyPacketType(TcpPacket.TcpHeader tcpHeader) {
        logger.trace("In identifyPacketType method");
        if (!tcpHeader.getUrg() && !tcpHeader.getRst() && !tcpHeader.getSyn() && !tcpHeader.getFin()
                && !tcpHeader.getPsh() && tcpHeader.getAck()) {
            addPacketInfoToLists(tcpHeader, 'A');
        } else if (tcpHeader.getAck() && tcpHeader.getPsh() && !tcpHeader.getRst() && !tcpHeader.getSyn()
                && !tcpHeader.getFin() && !tcpHeader.getUrg()) {
            addPacketInfoToLists(tcpHeader, 'P');
        }
    }

    private void addPacketInfoToLists(TcpPacket.TcpHeader tcpHeader, char type) {
        patternList.add(type);
        sequenceList.add(tcpHeader.getSequenceNumberAsLong());
        ackNoList.add(tcpHeader.getAcknowledgmentNumberAsLong());
    }

    public long determineElementOccurrence(LinkedList<Long> list, long elementToCheck) {
        return list.stream()
                .filter(element -> element == elementToCheck)
                .count();
    }

    private void addRequest() {
        logger.trace("In addRequest method");
        countRequests.getAndIncrement();
        listUtils.clearListExceptLastLong(ackNoList);
        listUtils.clearListExceptLastLong(sequenceList);
        listUtils.clearListExceptLastString(patternList);
        if (patternList.getLast() == 'P') {
            patternList.clear();
            ackNoList.clear();
            sequenceList.clear();
        }
    }


    public boolean isRequest(LinkedList<Character> patternList, int twoElementsBack, int threeElementsBack) {
        if (patternList.get(threeElementsBack) == 'A' && patternList.get(twoElementsBack) == 'P' &&
                patternList.getLast() == 'P') {
            return false;
        } else if (patternList.get(threeElementsBack) == 'A' && patternList.get(twoElementsBack) == 'P'
                && patternList.getLast() == 'A') {
            return false;
        } else if (patternList.get(threeElementsBack) == 'P' && patternList.get(twoElementsBack) == 'P'
                && patternList.getLast() == 'P') {
            return false;
        }
        return true;
    }

    private void comparePackets(int twoElementsBack, int threeElementsBack) {
        if (isRequest(patternList, twoElementsBack, threeElementsBack)) {
            addRequest();
        }
    }

}
