package com.qnext.trafficcounter.controller;

import com.qnext.trafficcounter.domain.Request;
import com.qnext.trafficcounter.services.RequestService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IndexController {

    private RequestService requestService;

    public IndexController(RequestService requestService) {
        this.requestService = requestService;
    }

    @RequestMapping("/req")
    public List<Request> getRequest() {
        return requestService.getRequests();
    }

}