package com.qnext.trafficcounter.config;

import com.qnext.trafficcounter.networkcontext.NetworkContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public NetworkContext networkContext() {
        return new NetworkContext();
    }
}
