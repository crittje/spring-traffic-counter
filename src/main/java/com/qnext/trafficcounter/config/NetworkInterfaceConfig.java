package com.qnext.trafficcounter.config;

import com.qnext.trafficcounter.networkcontext.ReadFiles;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.util.*;

public class NetworkInterfaceConfig {

    final private Logger logger = LogManager.getLogger();
    private HashMap<String, LinkedList<Integer>> sourcesMap = new HashMap<>();
    private Set<String> ipSet = new HashSet<>();

    public NetworkInterfaceConfig() {
        logger.trace("In constructor");

        readNetworkProperties();
    }

    private void readNetworkProperties() {
        logger.trace("in readNetworkProperties method");

        ReadFiles readFiles = new ReadFiles();
        Properties networkProperties = null;
        try {
            networkProperties = readFiles.configFiles("config/network.properties");
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        }

        String[] sources = networkProperties.getProperty("sources-list").split(",");
        logger.debug("Found {} sources", sources.length);

        for (String source : sources) {

            String ip = networkProperties.getProperty("source." + source + ".ip");

            Arrays.stream(networkProperties.getProperty("source." + source + ".port").split(","))
                    .forEach(portStr -> modifySourceMap(ip, Integer.parseInt(portStr.trim())));
        }
        logger.debug("SourceMap size {}", sources.length);
    }

    public void modifySourceMap(String ip, int port) {
        LinkedList<Integer> portsList;
        if (ipSet.contains(ip)) {
            portsList = sourcesMap.get(ip);
        } else {
            ipSet.add(ip);
            portsList = new LinkedList<>();
        }
        portsList.add(port);
        sourcesMap.put(ip, portsList);
    }

    public HashMap<String, LinkedList<Integer>> getSourcesMap() {
        return sourcesMap;
    }

    public Set<String> getIpSet() {
        return ipSet;
    }
}
