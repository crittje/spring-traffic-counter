package com.qnext.trafficcounter.utils;

import java.util.LinkedList;

public class ListUtils {

    public void clearListExceptLastLong(LinkedList<Long> list) {
        long last = list.getLast();
        list.clear();
        list.add(last);
    }

    public void clearListExceptLastString(LinkedList<Character> list) {
        Character last = list.getLast();
        list.clear();
        list.add(last);
    }

}
