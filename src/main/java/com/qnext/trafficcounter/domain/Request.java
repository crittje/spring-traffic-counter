package com.qnext.trafficcounter.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Request {

    @Id
    private Long id;

    private String sourceIp;

    private int port;

    private Long count;

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

}
