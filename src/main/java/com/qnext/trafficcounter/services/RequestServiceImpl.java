package com.qnext.trafficcounter.services;

import com.qnext.trafficcounter.networkcontext.NetworkContext;
import com.qnext.trafficcounter.domain.Request;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    private NetworkContext networkContext;

    public RequestServiceImpl(NetworkContext networkContext) {
        this.networkContext = networkContext;
    }

    @Override
    public List<Request> getRequests() {
        List<Request> requestList = new ArrayList<>();

        networkContext.getListenersList()
                .forEach(initListener -> {
                    Request request = new Request();
                    request.setSourceIp(initListener.getIp());
                    request.setPort(initListener.getPort());
                    request.setCount((long) initListener.getCountRequests().intValue());
                    requestList.add(request);
                });
        return requestList;
    }
}
