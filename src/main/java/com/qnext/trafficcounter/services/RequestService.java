package com.qnext.trafficcounter.services;

import com.qnext.trafficcounter.domain.Request;

import java.util.List;

public interface RequestService {

    List<Request> getRequests();
}
